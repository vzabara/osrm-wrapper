<?php
/**
 * GraphHopper Router Machine wrapper
 * by Vladimir Zabara <wlady2001@gmail.com>
 */

if (!function_exists('getallheaders'))  {
    function getallheaders() {
        if (!is_array($_SERVER)) {
            return array();
        }

        $headers = array();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

header('Content-Type: application/json');
if ($_SERVER['HTTP_ACCEPT'] && strtolower($_SERVER['HTTP_ACCEPT']) == 'application/json') {
    $config = require_once 'config.php';
    $apiKeyFile = __DIR__ . '/../api-keys.json';
    $code = 500;
    $message = 'Check service configuration';
    // available API keys
    if (file_exists($apiKeyFile)) {
        $apiKeys = (array)json_decode(file_get_contents($apiKeyFile));
        if (is_array($apiKeys)) {
            $code = 400;
            $message = 'API key is wrong';
            $headers = getallheaders();
            if (array_key_exists('Authorization', $headers)) {
                $parts = explode('=', $headers['Authorization']);
                if (count($parts)>1 && $apiKeys[$parts[1]]->active) {
                    $curl = curl_init();
                    $data = [
                        CURLOPT_URL => $config['server']['url'] . $_SERVER['REQUEST_URI'],
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => 'UTF-8',
                        CURLOPT_MAXREDIRS => 3,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => $_SERVER['REQUEST_METHOD'],
                        CURLOPT_HTTPHEADER => array(
                            'Cache-Control: no-cache',
                            'Content-Type: application/json'
                        ),
                    ];
                    curl_setopt_array($curl, $data);
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    if ($err) {
                        $code = 500;
                        $message = 'Service error';
                    } else {
                        // return response from GraphHopper server AS IS
                        echo $response;
                        die();
                    }
                }
            }
        }
    }
    echo json_encode([
        'error' => true,
        'message' => $message,
    ]);
} else {
    $code = 200;
    echo json_encode([
        'info' => 'Router Machine wrapper',
    ]);
}
die($code);

